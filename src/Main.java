import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[] array = {"elephant", "chicken", "cat", "japanese bee", "snake"};
        System.out.println("Initial array: " + Arrays.toString(array));
        System.out.println("--------");
        insertSort(array);
        System.out.println("Sorted array: " + Arrays.toString(array));
    }

    public static void insertSort(String[] array){
        for(int i = 1; i < array.length; i++){
            String currentValue = array[i];
            int j = i - 1;
            while(j >= 0 && countChar(array[j]) > countChar(currentValue)){
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = currentValue;
        }
    }

    public static int countChar(String string){
        int count = 0;
        for(int i = 0; i < string.length(); i++){
            if(string.charAt(i) == 'a' || string.charAt(i) == 'e' || string.charAt(i) == 'i' || string.charAt(i) == 'u' || string.charAt(i) == 'o'){
                count++;
            }
        }
        return count;
    }
}